import { Input, message } from "antd";
import type { NextPage } from "next";
import getConfig from "next/config";
import Head from "next/head";
import React, { useEffect, useState } from "react";
import Pagination from "../components/pagination";
import DriverCard from "../containers/driver-card";
import homeStyles from "../styles/home.module.css";
import { Driver } from "../types/driver";

const PAGE_SIZE = 5;

const Home: NextPage = () => {
  const { publicRuntimeConfig } = getConfig();
  const { apiUrl } = publicRuntimeConfig;

  const [drivers, setDrivers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [keyword, setKeyword] = useState("");

  async function fetchDrivers() {
    try {
      const params = new URLSearchParams({
        page: `${currentPage}`,
        results: `${PAGE_SIZE}`,
        keyword,
      });
      const res = await fetch(`${apiUrl}?${params}`);
      const { results } = await res.json();

      setDrivers(results);
    } catch {
      message.error("An error occurred, please try again later.");
    }
  }

  useEffect(() => {
    fetchDrivers();
  }, [currentPage, keyword]);

  const onSearch = (value: string) => setKeyword(value);

  const onPaginationChange = (current: number) => setCurrentPage(current);

  const { Search } = Input;

  return (
    <>
      <Head>
        <title>Driver Management</title>
        <meta name="description" content="Driver Management" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className={homeStyles.header}>
          <div>
            <h1 className={homeStyles.header__title}>Driver Management</h1>
            <p className={homeStyles.header__subtitle}>
              Data driver yang bekerja dengan Anda.
            </p>
          </div>

          <div className={homeStyles.search__input}>
            <Search placeholder="Cari Driver" onSearch={onSearch} enterButton />
          </div>
        </div>

        <div data-testid="driver-list" className={homeStyles.driver__list}>
          {drivers.map((driver: Driver) => (
            <DriverCard key={driver.login.uuid} driver={driver} />
          ))}
        </div>
        <Pagination
          current={currentPage}
          total={30}
          pageSize={PAGE_SIZE}
          handleClick={onPaginationChange}
        />
      </main>
    </>
  );
};

export default Home;
