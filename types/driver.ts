export interface Driver {
  login: {
    uuid: string;
  };
  id: {
    value: string;
  };
  picture: {
    large: string;
  };
  name: {
    first: string;
    last: string;
  };
  phone: string;
  email: string;
  dob: {
    date: string;
  };
}
