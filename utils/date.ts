import dayjs from "dayjs";

export const format = (dateString: string, template: string = "DD-MM-YYYY") =>
  dayjs(dateString).format(template);
