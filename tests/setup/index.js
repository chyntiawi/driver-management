import "@testing-library/jest-dom";
import "whatwg-fetch";

jest.mock("next/config", () => () => ({
  publicRuntimeConfig: {
    apiUrl: "https://randomuser.me/api",
  },
}));
