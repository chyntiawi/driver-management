import { rest } from "msw";
import { setupServer } from "msw/node";
import { render, screen, waitFor } from "@testing-library/react";
import Home from "../../pages/index";

describe("Driver Management Page", () => {
  const apiUrl = "https://randomuser.me/api";
  const server = setupServer(
    rest.get(apiUrl, (_: any, res: any, context: any) => {
      return res(
        context.json({
          results: [
            {
              login: {
                uuid: "2bf0b-8188-4445-9d52",
              },
              id: {
                value: "2181867T",
              },
              picture: {
                large: "https://randomuser.me/api/portraits/men/1.jpg",
              },
              name: {
                first: "Antonio",
                last: "Prieto",
              },
              phone: "932-846-655",
              email: "antonio.prieto@example.com",
              dob: {
                date: "1993-07-07T02:49:42.807Z",
              },
            },
            {
              login: {
                uuid: "9bed6f-a039-52329fc8",
              },
              id: {
                value: "478006139",
              },
              picture: {
                large: "https://randomuser.me/api/portraits/men/2.jpg",
              },
              name: {
                first: "Manuel",
                last: "Morel",
              },
              phone: "061-612-0703",
              email: "manuel.morel@example.com",
              dob: {
                date: "2002-10-10T04:41:52.582Z",
              },
            },
          ],
        })
      );
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  describe("when fetch drivers is success", () => {
    it("renders driver list correctly", async () => {
      render(<Home />);

      await waitFor(async () => {
        expect(
          (await screen.findByTestId("driver-list")).childElementCount
        ).toBe(2);
      });
    });
  });

  describe("when fetch drivers is failed", () => {
    it("renders an error message", async () => {
      server.use(
        rest.get(apiUrl, (_: any, res: any, context: any) => {
          return res(context.status(500));
        })
      );

      render(<Home />);

      expect(
        await screen.findByText("An error occurred, please try again later.")
      ).toBeInTheDocument();
    });
  });
});
