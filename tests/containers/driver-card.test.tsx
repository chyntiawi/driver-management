import { render, screen, waitFor } from "@testing-library/react";
import DriverCard from "../../containers/driver-card";

describe("Driver Card Container", () => {
  const mockProps = {
    login: {
      uuid: "2bf0b-8188-4445-9d52",
    },
    id: {
      value: "2181867T",
    },
    picture: {
      large: "https://randomuser.me/api/portraits/men/17.jpg",
    },
    name: {
      first: "Antonio",
      last: "Prieto",
    },
    phone: "932-846-655",
    email: "antonio.prieto@example.com",
    dob: {
      date: "1993-07-07T02:49:42.807Z",
    },
  };

  const testCases = [
    ["driver-id", "Driver ID 2181867T"],
    ["driver-name", "Antonio, Prieto"],
    ["driver-phone", "932-846-655"],
    ["driver-email", "antonio.prieto@example.com"],
    ["driver-birth-date", "07-07-1993"],
  ];

  it.each(testCases)("renders %s correctly", async (testId, expected) => {
    render(<DriverCard driver={mockProps} />);

    expect(await screen.findByTestId(testId)).toHaveTextContent(expected);
  });

  it("renders driver photo correctly", async () => {
    render(<DriverCard driver={mockProps} />);

    const driverPhoto = await screen.findByTestId("driver-photo");

    await waitFor(() =>
      expect(driverPhoto.getAttribute("src")).toContain(
        encodeURIComponent(mockProps.picture.large)
      )
    );
  });
});
