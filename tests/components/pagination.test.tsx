import { fireEvent, render, screen } from "@testing-library/react";
import Pagination from "../../components/pagination";

describe("Pagination Component", () => {
  const mockProps = {
    current: 1,
    total: 10,
    pageSize: 5,
    handleClick: jest.fn(),
  };

  describe("when the page first load", () => {
    it("should disabled 'Previous Page' button", async () => {
      render(<Pagination {...mockProps} />);

      const prevBtn = await screen.findByTestId("prev-button");

      expect(prevBtn).toBeDisabled();
    });

    it("should enabled 'Next Page' button", async () => {
      render(<Pagination {...mockProps} />);

      const nextBtn = await screen.findByTestId("next-button");

      expect(nextBtn).not.toBeDisabled();
    });
  });

  describe("click 'Next Page' button", () => {
    it("should passing current increment to 'handleClick' callback function", async () => {
      render(<Pagination {...mockProps} />);

      const nextBtn = await screen.findByTestId("next-button");
      fireEvent.click(nextBtn);

      expect(mockProps.handleClick).toBeCalledWith(2);
    });

    describe("then click 'Previous Page' button", () => {
      it("should passing current decrement to 'handleClick' callback function", async () => {
        render(<Pagination {...mockProps} current={2} />);

        const prevBtn = await screen.findByTestId("prev-button");
        fireEvent.click(prevBtn);

        expect(mockProps.handleClick).toBeCalledWith(1);
      });
    });
  });

  describe("when it is the last page", () => {
    const mockPropsLastPage = {
      ...mockProps,
      current: 2,
    };

    it("should disabled 'Next Page' button", async () => {
      render(<Pagination {...mockPropsLastPage} />);

      const nextBtn = await screen.findByTestId("next-button");

      expect(nextBtn).toBeDisabled();
    });

    it("should enabled 'Previous Page' button", async () => {
      render(<Pagination {...mockPropsLastPage} />);

      const prevBtn = await screen.findByTestId("prev-button");

      expect(prevBtn).not.toBeDisabled();
    });
  });
});
