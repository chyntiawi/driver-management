import { Button } from "antd";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import paginationStyles from "../styles/pagination.module.css";

interface PaginationProps {
  current: number;
  total: number;
  pageSize: number;
  handleClick: (currentPage: number) => void;
}

export default function Pagination({
  current,
  total,
  pageSize,
  handleClick,
}: PaginationProps) {
  const onPrevClick = () => handleClick(current - 1);

  const onNextClick = () => handleClick(current + 1);

  return (
    <div className={paginationStyles.pagination}>
      <Button
        data-testid="prev-button"
        type="text"
        disabled={current === 1}
        onClick={onPrevClick}
      >
        <LeftOutlined /> Previous Page
      </Button>
      <Button
        data-testid="next-button"
        type="text"
        disabled={current === total / pageSize}
        onClick={onNextClick}
      >
        Next Page <RightOutlined />
      </Button>
    </div>
  );
}
