import { Card, Divider } from "antd";
import Image from "next/image";
import driverCardStyles from "../styles/driver-card.module.css";
import { Driver } from "../types/driver";
import { format as formatDate } from "../utils/date";

interface DriverCardProps {
  driver: Driver;
}

export default function DriverCard({ driver }: DriverCardProps) {
  const { id, picture, name, phone, email, dob } = driver;
  const data = [
    {
      title: "Nama Driver",
      subtitle: `${name.first}, ${name.last}`,
      testId: "driver-name",
    },
    {
      title: "Telepon",
      subtitle: phone,
      testId: "driver-phone",
    },
    {
      title: "Email",
      subtitle: email,
      testId: "driver-email",
    },
    {
      title: "Tanggal Lahir",
      subtitle: formatDate(dob.date),
      testId: "driver-birth-date",
    },
  ];

  return (
    <div className={driverCardStyles.card}>
      <Card bordered={false}>
        <p data-testid="driver-id" className={driverCardStyles.header}>
          Driver ID{" "}
          <span className={driverCardStyles.driver__id}>{id.value}</span>
        </p>
        <Divider />
        <Image
          data-testid="driver-photo"
          className={driverCardStyles.driver__image}
          src={picture.large}
          alt="driver photo"
          width={80}
          height={80}
        />
        <div className={driverCardStyles.body}>
          {data.map(({ title, subtitle, testId }) => (
            <div key={testId} data-testid={testId}>
              <p className={driverCardStyles.title}>{title}</p>
              <p className={driverCardStyles.subtitle}>{subtitle}</p>
            </div>
          ))}
        </div>
      </Card>
    </div>
  );
}
